# Intro to CSS

## Objectives

Students will learn how CSS makes HTML _look fantastic_ and will practice writing basic CSS so they can apply their own custom styling to HTML.

## SWBATS

+ CSS - Write simple sets of style rules to change the look of HTML
+ CSS - Link CSS in multiple ways

## Introduction to CSS, A.K.A Cascading Style Sheets

Back in the early days of the internet, web pages were built out of _only_ HTML.  It worked fine - we could use `<h1>` tags for header sections, `<p>` tags to hold our text content, `<img>` tags to display images, etc. With a lot of trial and error, could get to a functioning website.  However, it was all [_very plain_][min-rms-style]. Text styling was limited to things like **bold** and _italic_... not very exciting, but it's all we had! On the modern internet, HTML is still necessary, but it is mostly used for setting up the structure of a web page. In order to make modern web pages look as good as they do, CSS was introduced to work side by side with HTML.  CSS has the ability to totally change the design, and therefore the experience, a user has when visiting a website.

[Here is an example of some plain old HTML and what just a little bit of CSS can do!](https://ironboard-learn.s3.amazonaws.com/klossy_basic_html_example.html)

CSS, or Cascading Style Sheets, is its own language that defines how HTML is displayed on a page. It has its own rules for syntax that we will learn, but once you know the basics, you'll have the ability to style HTML in limitless ways. For example, take a look at this extreme [example of styling taken to the stylish max][max-style]. 

Alright, CSS is pretty exciting! It adds an aesthetic layer to our websites. Before we start practicing, let's examine how we can incorporate CSS into our HTML.

## Incorporating CSS

**NOTE:** Each of these should be shown to the students in a real-time example as you are going through them. Borders/background colors also make for useful visual styles to show students, especially when demonstrating width/height properties.

CSS can be used in our HTML in **three** ways:

--- 

#### 1. Inline

CSS rules can be written directly inside HTML tags. We call this 'inline styling'. For example, we can make the text in the following `<p>` element blue, and its width 50px, by writing the following:

```html
<p style="color: blue; width: 50px;">This text will show up blue.</p>
```
#### CFU

Have students go to  [jsfiddle](https://jsfiddle.net/). Tell them to guess how they might change the text color in this way, have them try to change the text to a few different colors starting with the given HTML, clicking the "run" button to get going.  

Demonstrate how only applies to one element: 

```html
<p style="color: blue;">This text will show up blue.</p>
<p>This text will not show up blue.</p>
```

---

#### 2. Internal

CSS rules can also be written directly in an HTML file. Using a `<style>` tag inside the `<head>` of an HTML file, we can write something like:

```html
<html>
  <head>
    <style>
      p { 
        color: blue;
        width: 50px;
      }
    </style>
  </head>
  
  <body>
    <p>Hello!</p>
  </body>
</html>
```

The example above will alter _all_ `<p>` elements to have blue text and be 50 pixels wide. This is in contrast to the first example, which will only apply the styling to the specific element.

---

#### 3. External

CSS rules can be written in a separate `.css` file, which is then _linked_ in an HTML page. This is the most common way websites utilize CSS:
```html
<html>
  <head>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <p>Hello!</p>
  </body>
</html>
```

...and in a separate `style.css` file...

```css
p { 
  color: blue;
  width: 50px;
}
```

Writing our styling in `.css` files allows us to avoid writing the same thing over and over as we can re-use it in many different HTML files. Additionally, this keeps our HTML pages a cleaner and easier to read. This method (linking to CSS files) is the standard practice we will follow when incorporating CSS into our websites. 

#### CFU

More opportuniteis to play around with JS Fiddle! Maybe make a more narrowed down list of [css properties](https://www.w3schools.com/cssref/default.asp). Use the basic html of just 

```html
<p>Hello! How are you today?</p>
```

and having them pick properites and assign values in jsfiddle and see what (if anything) happens.

---

## CSS Rule Heirarchy

The first word in CSS, _Cascading_, means that when a rule in CSS is read by your browser, it overrides any previous settings for that particular CSS rule. HTML reads from the top down, so if you've defined the color of text in a `<p>` tag as blue, but then a few lines down, defined the color of text in a `<p>` tag to red, text will appear red, not blue:

```css
/* style.css */

p {
  color: blue;
}

p {
  color: red;
}

/* Red it is! */
```

One thing that is important to note: There are _hundreds_ of CSS properties. We won't be concerned with most of them, and we don't have enough time to go deep into CSS practices, but that is okay.  As we mentioned before, _as long as_ you have an understanding the syntax for writing CSS rules and understand how to link CSS to HTML, you have all the skills you need to make HTML _look great_.

## Getting Started with CSS

Learning CSS is definitely a hands-on process, so the code-along linked below should be used by each student as we go through some of the CSS basics as a class.

[The Ultimate CSS Walkthrough](https://github.com/learn-co-curriculum/kwk-l1-css-walkthrough-code-along)

**NOTE:** The teacher should also clone the above repository so students can follow along as a group. Make sure you are familiar with the flow and not doing it for the first time with the students! The text on `index.html` can be read aloud and provides a good guide to teach with. Additional challenges are provided as well as suggestions.

## Building Our Own Websites

HTML and CSS are all you need to build a great looking website.  Building a website is also a great way to get better at HTML and CSS!  This site can be about anything, and should use all of the concepts we've learned today.  Use a variety of HTML tags and CSS classes to create different sections on the page.

[Building My Own HTML Website](https://github.com/learn-co-curriculum/kwk-l1-my-own-html-website)

During this project, you can use any HTML elements you like. 

**NOTE:** students should first plan out their site, spend some time thinking about what should be on the site, what HTML elements they'll need and in what order. Without giving them the answer straight out, help guide the students who forget to link their CSS in the HTML file.

CSS Examples for getting off the ground:
```css
body {
  /* sets background image. Can also play animated gifs */
  background: url(an_image_OR_animated_gif_from_the_internet);
  background-size: contain;
}

h1 {
  /* centered, very large header */
  text-align: center;
  font-size: 400%;
}

.navigation {
  /* creates bar across screen if class is assigned to HTML */
  width: 100%;
  height: 50px;
  background-color: lightblue;
}
```

### References:

* [CSS Full Reference Sheet](https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/d7fb67af-5180-463d-b58a-bfd4a220d5d0/css3-cheat-sheet.pdf)
* [CSS Reference](https://www.w3schools.com/cssref/default.asp)

### Additional CSS Labs:

* [CSS Selectors](https://github.com/learn-co-curriculum/upperline-hs-intro-software-engineering-css-selectors)
* [Empire State Mock Site](https://github.com/learn-co-curriculum/upperline-hs-empire-state-css-challenge)
* [Zetsy](https://github.com/learn-co-curriculum/kwk-l1-zetsy)

[max-style]:http://karimrashid.com/overview#about
[min-rms-style]: https://stallman.org/rms-lifestyle.html
